package er.mayak.mutabor.mayak.model;


import java.util.Date;

public class Item {

    private final String datePattern = "E, dd/MM/yyyy";

    private String title;
    private String description;
    private String mp3Url;
    private String pubDate;
    private String duration;

    public Item(String title, String description, String mp3Url, String pubDate, String duration){
        this.title = title;
        this.description = description;
        this.mp3Url = mp3Url;
        this.duration = duration;
        String dateFormat = (String) android.text.format.DateFormat.format(datePattern, new Date(pubDate));
        this.pubDate = dateFormat;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMp3Url() {
        return mp3Url;
    }

    public void setMp3Url(String mp3Url) {
        this.mp3Url = mp3Url;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String toString(){
        return getTitle()+" : "+ getDescription()+" : "+getMp3Url()+" : "+getPubDate()+" : "+getDuration();
    }
}
