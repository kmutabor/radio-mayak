package er.mayak.mutabor.mayak;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;


public class PlayerActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = "TAG_" + getClass().getSimpleName();

    private SeekBar seekBar;
    private AudioManager audioManager;
    private int maxVolume;
    private int curVolume;
    private SimpleExoPlayer mediaPlayer;
    private Button btnPlay;
    private Button btnPause;
    private TextView podcastNameTv;
    private TextView durationTv;

    private String url, title, duration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_activity);

        initialiseSeekBar();

        btnPlay = (Button)findViewById(R.id.btn_play);
        btnPause = (Button)findViewById(R.id.btn_pause);

        btnPlay.setOnClickListener(this);
        btnPause.setOnClickListener(this);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        url = intent.getStringExtra("url");
        duration = intent.getStringExtra("duration");

        podcastNameTv = (TextView)findViewById(R.id.podcast_name_tv);
        podcastNameTv.setText(title);

        durationTv = (TextView)findViewById(R.id.duration_tv);
        durationTv.setText(duration);

        initAndPlayPlayer(url);

    }



    private void initialiseSeekBar() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        seekBar = (SeekBar) findViewById(R.id.volume);
        seekBar.setMax(maxVolume);
        seekBar.setProgress(curVolume);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {}

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {}

            @Override
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, arg1, 0);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_play:
                if(!isPlayed()){
                    play();
                }
                podcastNameTv.setText(title);
                break;
            case R.id.btn_pause:
                if(isPlayed()){
                    pause();
                }
                podcastNameTv.setText(getResources().getString(R.string.pause_text));
                break;
        }
    }


    /**
     * @param stream
     */
    private void initAndPlayPlayer(String stream) {
        ((App)getApplicationContext()).releasePlayer();

        Handler mHandler = new Handler();
        String userAgent = "Mutabor Mayak App";
        Uri uri = Uri.parse(stream);
        DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        MediaSource mediaSource = new ExtractorMediaSource(uri, dataSourceFactory, Mp3Extractor.FACTORY, mHandler, null);

        mediaPlayer = ((App)getApplicationContext()).getGlobalMediaPlayer();
        mediaPlayer.prepare(mediaSource);
        mediaPlayer.setPlayWhenReady(true);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

    }



    private boolean isPlayed(){
        if (mediaPlayer != null) {
            if(mediaPlayer.getPlayWhenReady()){
                return true;
            }
        }
        return false;
    }

    private void play(){
        mediaPlayer.setPlayWhenReady(true);
    }
    private void pause(){
        mediaPlayer.setPlayWhenReady(false);
    }

}
