package er.mayak.mutabor.mayak;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import er.mayak.mutabor.mayak.model.Item;


public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {
    private final String TAG = "TAG_" + getClass().getSimpleName();

    List<Item> items;
    ItemAdapter(List<Item> items){
        this.items = items;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView title;
        TextView description;
        TextView pubDate;
        TextView duration;
        ItemHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            title = (TextView)itemView.findViewById(R.id.item_title);
            description = (TextView)itemView.findViewById(R.id.item_description);
            pubDate = (TextView)itemView.findViewById(R.id.item_pub_date);
            duration = (TextView)itemView.findViewById(R.id.item_duration);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ItemAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        ItemHolder itemHolder = new ItemHolder(v);
        return itemHolder;
    }

    @Override
    public void onBindViewHolder(ItemAdapter.ItemHolder holder, int position) {
        final Item item = this.items.get(position);
        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        holder.pubDate.setText(item.getPubDate());
        holder.duration.setText(item.getDuration());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

