package er.mayak.mutabor.mayak.util;

import java.util.ArrayList;
import java.util.List;

import er.mayak.mutabor.mayak.model.Source;

public final class SourceConstants {

    public static final int THE_CHILDREN = 0;

    public static final int ADMIRALTEYSTVO = 1;
    public static final int ASSAMBLEYA_AVTOMOBILISTOV = 2;

    public static final int BELAYA_STUDIA = 3;
    public static final int BOLSHOY_TEST_DRIVE = 4;
    public static final int BRENDYATINA = 5;

    public static final int VELIKIY_XIX = 6;
    public static final int VIA = 7;
    public static final int VNEKLASSNOE_4TENIE = 8;

    public static final int GOVORIM_PRAVILNO = 9;

    public static final int DK_URAL = 10;

    public static final int ZH_Z_I = 11;

    public static final int I_PUST_VES_HAIP_PODOZHDET = 12;
    public static final int IMENEM_REVOLUCII = 13;
    public static final int INOVESHANIE = 14;

    public static final int KAK_ZARABOTAT_MILLION = 15;
    public static final int KLINIKA_FADEEVA = 16;
    public static final int KLUB_RADIOPUTESHESTVENNIKOV = 17;
    public static final int KNIZHNAYA_POLKA = 18;
    public static final int KRASNAYA_MASHINA = 19;
    public static final int KRILYA_SOVETOV = 20;
    public static final int KURS_POVISHENIYA_ZNANIY = 21;

    public static final int LEKTORIUM = 22;
    public static final int LIKVIDACIYA_BEZGRAMOTNOSTI = 23;
    public static final int LITVIN = 24;
    public static final int LITERATURNIY_NOBEL = 25;

    public static final int MALENKIE_ISTORII_BOLSHOY_STRANI = 26;
    public static final int MASTERA_SPORTA_FUTBOL = 27;
    public static final int MAYAKOVSHINA_VNE_EFIRA = 28;
    public static final int MODA_STIL = 29;
    public static final int MODNAYA_SREDA = 30;
    public static final int MOZG = 31;
    public static final int MOSKVA_SLEZAM_POVERIT = 32;
    public static final int MOSKVOVEDENIE = 33;
    public static final int MUZH4INA_RUKOVODSTVO_PO_EKSPLUATACII = 34;
    public static final int MUZIKANTI = 35;

    public static final int NEIZVESTNOE_OB_IZVESTNOM = 36;

    public static final int O_VKUSNOY_I_ZDOROVOY_PISHE = 37;
    public static final int OB_ETOM_NE_PISALI_V_GAZETAH = 38;

    public static final int POL_KINO = 39;
    public static final int POPULARNAYA_EKONOMIKA = 40;
    public static final int PORA_DOMOY = 41;

    public static final int REBYATAM_O_ZVERYATAH = 42;
    public static final int RODITELSKIY_CHAS = 43;
    public static final int ROSA_VETROV = 44;

    public static final int SERGEY_STILLAVIN_I_EGO_DRUZYA = 45;
    public static final int SLADKAYA_ZHIZN = 46;
    public static final int SOBRANIE_SLOV = 47;
    public static final int SPUTNIK_KINOZRITELYA_S_ANTONOM_DOLINIM = 48;
    public static final int STAHOVSKIY_LIVE = 49;
    public static final int STRANA_TRANSISTORIYA = 50;

    public static final int TAYNA_OKEANA = 51;
    public static final int TOVARISH_TCIN = 52;
    public static final int TOLKOVIY_SLOVAR = 53;

    public static final int UTRO_KARL = 54;

    public static final int PHILOSOPHY = 55;

    public static final int HOCHU_VSE_ZNAT = 56;
    public static final int HOCHU_VSE_ZNAT_ASTRONOMIYA = 57;
    public static final int HOCHU_VSE_ZNAT_BIOLOGIYA = 58;
    public static final int HOCHU_VSE_ZNAT_GEOGRAFIYA = 59;
    public static final int HOCHU_VSE_ZNAT_ISKUSSTVO = 60;
    public static final int HOCHU_VSE_ZNAT_ISTORIYA = 61;
    public static final int HOCHU_VSE_ZNAT_KULTUROLOGIYA = 62;
    public static final int HOCHU_VSE_ZNAT_LITERATUROVEDENIE = 63;
    public static final int HOCHU_VSE_ZNAT_MATEMATIKA = 64;
    public static final int HOCHU_VSE_ZNAT_OBZH = 65;
    public static final int HOCHU_VSE_ZNAT_OKRUZHAUSHIY_MIR = 66;
    public static final int HOCHU_VSE_ZNAT_RAZVITIE_MISHLENIYA = 67;
    public static final int HOCHU_VSE_ZNAT_RITORIKA = 68;
    public static final int HOCHU_VSE_ZNAT_RUSSKIY_YAZIK = 69;
    public static final int HOCHU_VSE_ZNAT_FIZIKA = 70;
    public static final int HOCHU_VSE_ZNAT_HIMIYA = 71;
    public static final int HOCHU_VSE_ZNAT_SHKOLA_ROKA = 72;

    public static final int CHTENIE = 73;
    public static final int CHTENIE_DLYA_DETEY = 74;

    public static final int _23_FEVRALYA = 75;
    public static final int _30_SUZHETOV_CIVILIZACII = 76;
    public static final int _4_SEZONA = 77;


    /*
    ниже архивные записи
     */
    public static final int AKUSTICHESKAYA_SREDA = 78;



    public static final List<Source> SOURCE_LIST = new ArrayList<>();
    static {
        SOURCE_LIST.add(SourceConstants.THE_CHILDREN, new Source("The Детки", "https://radiomayak.ru/podcasts/rss/podcast/922/brand/59285/type/audio/"));

        SOURCE_LIST.add(SourceConstants.ADMIRALTEYSTVO, new Source("Адмиралтейство", "https://radiomayak.ru/podcasts/rss/podcast/2001/brand/59986/type/audio/"));
        SOURCE_LIST.add(SourceConstants.ASSAMBLEYA_AVTOMOBILISTOV, new Source("Ассамблея автомобилистов", "https://radiomayak.ru/podcasts/rss/podcast/1341/type/audio/"));

        SOURCE_LIST.add(SourceConstants.BELAYA_STUDIA, new Source("Белая студия", "https://radiomayak.ru/podcasts/rss/podcast/1621/brand/60200/type/audio/"));
        SOURCE_LIST.add(SourceConstants.BOLSHOY_TEST_DRIVE, new Source("Большой тест-драйв", "https://radiomayak.ru/podcasts/rss/podcast/48/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.BRENDYATINA, new Source("Брендятина", "https://radiomayak.ru/podcasts/rss/podcast/45/brand/58219/type/audio/"));

        SOURCE_LIST.add(SourceConstants.VELIKIY_XIX, new Source("Великий XIX", "https://radiomayak.ru/podcasts/rss/podcast/1521/type/audio/"));
        SOURCE_LIST.add(SourceConstants.VIA, new Source("ВИА", "https://radiomayak.ru/podcasts/rss/podcast/2021/brand/61098/type/audio/"));
        SOURCE_LIST.add(SourceConstants.VNEKLASSNOE_4TENIE, new Source("Внеклассное чтение", "https://radiomayak.ru/podcasts/rss/podcast/2121/brand/61222/type/audio/"));

        SOURCE_LIST.add(SourceConstants.GOVORIM_PRAVILNO, new Source("Говорим правильно", "https://radiomayak.ru/podcasts/rss/podcast/1721/brand/59037/type/audio/"));

        SOURCE_LIST.add(SourceConstants.DK_URAL, new Source("ДК Урал", "https://radiomayak.ru/podcasts/rss/podcast/1701/brand/59986/type/audio/"));

        SOURCE_LIST.add(SourceConstants.ZH_Z_I, new Source("Жизнь замечательных идей", "https://radiomayak.ru/podcasts/rss/podcast/2142/brand/61222/type/audio/"));

        SOURCE_LIST.add(SourceConstants.I_PUST_VES_HAIP_PODOZHDET, new Source("И пусть весь хайп подождёт", "https://radiomayak.ru/podcasts/rss/podcast/2621/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.IMENEM_REVOLUCII, new Source("Именем революции!", "https://radiomayak.ru/podcasts/rss/podcast/2201/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.INOVESHANIE, new Source("Иновещание", "https://radiomayak.ru/podcasts/rss/podcast/921/brand/59269/type/audio/"));

        SOURCE_LIST.add(SourceConstants.KAK_ZARABOTAT_MILLION, new Source("Как заработать миллион?", "https://radiomayak.ru/podcasts/rss/podcast/582/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KLINIKA_FADEEVA, new Source("Клиника Фадеева", "https://radiomayak.ru/podcasts/rss/podcast/742/brand/59037/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KLUB_RADIOPUTESHESTVENNIKOV, new Source("Клуб радиопутешественников", "https://radiomayak.ru/podcasts/rss/podcast/1421/brand/59037/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KNIZHNAYA_POLKA, new Source("Книжная полка", "https://radiomayak.ru/podcasts/rss/podcast/263/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KRASNAYA_MASHINA, new Source("Красная машина", "https://radiomayak.ru/podcasts/rss/podcast/2281/brand/61254/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KRILYA_SOVETOV, new Source("Крылья советов", "https://radiomayak.ru/podcasts/rss/podcast/1681/brand/59986/type/audio/"));
        SOURCE_LIST.add(SourceConstants.KURS_POVISHENIYA_ZNANIY, new Source("Курс повышения знаний", "https://radiomayak.ru/podcasts/rss/podcast/2141/brand/61222/type/audio/"));

        SOURCE_LIST.add(SourceConstants.LEKTORIUM, new Source("Лекториум", "https://radiomayak.ru/podcasts/rss/podcast/2061/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.LIKVIDACIYA_BEZGRAMOTNOSTI, new Source("Ликвидация безграмотности", "https://radiomayak.ru/podcasts/rss/podcast/1261/type/audio/"));
        SOURCE_LIST.add(SourceConstants.LITVIN, new Source("Литвин", "https://radiomayak.ru/podcasts/rss/podcast/2321/brand/59286/type/audio/"));
        SOURCE_LIST.add(SourceConstants.LITERATURNIY_NOBEL, new Source("Литературный Нобель", "https://radiomayak.ru/podcasts/rss/podcast/1501/brand/59038/type/audio/"));

        SOURCE_LIST.add(SourceConstants.MALENKIE_ISTORII_BOLSHOY_STRANI, new Source("Маленькие истории большой страны", "https://radiomayak.ru/podcasts/rss/podcast/2101/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MASTERA_SPORTA_FUTBOL, new Source("Мастера спорта. Футбол", "https://radiomayak.ru/podcasts/rss/podcast/1461/brand/58224/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MAYAKOVSHINA_VNE_EFIRA, new Source("Маяковщина. Вне эфира", "https://radiomayak.ru/podcasts/rss/podcast/2601/brand/61955/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MODA_STIL, new Source("Мода. Стиль", "https://radiomayak.ru/podcasts/rss/podcast/1221/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MODNAYA_SREDA, new Source("Модная среда", "https://radiomayak.ru/podcasts/rss/podcast/2221/brand/59285/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MOZG, new Source("Мозг", "https://radiomayak.ru/podcasts/rss/podcast/741/brand/59038/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MOSKVA_SLEZAM_POVERIT, new Source("Москва слезам поверит", "https://radiomayak.ru/podcasts/rss/podcast/1181/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MOSKVOVEDENIE, new Source("Москвоведение", "https://radiomayak.ru/podcasts/rss/podcast/1801/brand/59285/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MUZH4INA_RUKOVODSTVO_PO_EKSPLUATACII, new Source("Мужчина. Руководство по эксплуатации", "https://radiomayak.ru/podcasts/rss/podcast/1641/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.MUZIKANTI, new Source("Музыканты", "https://radiomayak.ru/podcasts/rss/podcast/801/type/audio/"));

        SOURCE_LIST.add(SourceConstants.NEIZVESTNOE_OB_IZVESTNOM, new Source("Неизвестное об известном", "https://radiomayak.ru/podcasts/rss/podcast/1201/brand/59037/type/audio/"));

        SOURCE_LIST.add(SourceConstants.O_VKUSNOY_I_ZDOROVOY_PISHE, new Source("О вкусной и здоровой пище", "https://radiomayak.ru/podcasts/rss/podcast/2741/brand/61642/type/audio/"));
        SOURCE_LIST.add(SourceConstants.OB_ETOM_NE_PISALI_V_GAZETAH, new Source("Об этом не писали в газетах", "https://radiomayak.ru/podcasts/rss/podcast/2041/brand/58219/type/audio/"));

        SOURCE_LIST.add(SourceConstants.POL_KINO, new Source("Полкино", "https://radiomayak.ru/podcasts/rss/podcast/2081/brand/58229/type/audio/"));
        SOURCE_LIST.add(SourceConstants.POPULARNAYA_EKONOMIKA, new Source("Популярная экономика", "https://radiomayak.ru/podcasts/rss/podcast/2781/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.PORA_DOMOY, new Source("#порадомой", "https://radiomayak.ru/podcasts/rss/podcast/2721/brand/62044/type/audio/"));

        SOURCE_LIST.add(SourceConstants.REBYATAM_O_ZVERYATAH, new Source("Ребятам о зверятах", "https://radiomayak.ru/podcasts/rss/podcast/2102/brand/59037/type/audio/"));
        SOURCE_LIST.add(SourceConstants.RODITELSKIY_CHAS, new Source("Родительский час", "https://radiomayak.ru/podcasts/rss/podcast/2182/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.ROSA_VETROV, new Source("Роза ветров", "https://radiomayak.ru/podcasts/rss/podcast/1961/brand/59986/type/audio/"));

        SOURCE_LIST.add(SourceConstants.SERGEY_STILLAVIN_I_EGO_DRUZYA, new Source("Сергей Стиллавин и его друзья", "https://radiomayak.ru/podcasts/rss/podcast/2441/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.SLADKAYA_ZHIZN, new Source("Сладкая жизнь", "https://radiomayak.ru/podcasts/rss/podcast/1981/brand/59986/type/audio/"));
        SOURCE_LIST.add(SourceConstants.SOBRANIE_SLOV, new Source("Собрание слов", "https://radiomayak.ru/podcasts/rss/podcast/561/brand/58709/type/audio/"));
        SOURCE_LIST.add(SourceConstants.SPUTNIK_KINOZRITELYA_S_ANTONOM_DOLINIM, new Source("Спутник кинозрителя с Антоном Долиным", "https://radiomayak.ru/podcasts/rss/podcast/118/type/audio/"));
        SOURCE_LIST.add(SourceConstants.STAHOVSKIY_LIVE, new Source("Стаховский LIVE", "https://radiomayak.ru/podcasts/rss/podcast/2541/brand/59038/type/audio/"));
        SOURCE_LIST.add(SourceConstants.STRANA_TRANSISTORIYA, new Source("Страна Транзистория", "https://radiomayak.ru/podcasts/rss/podcast/2821/brand/59986/type/audio/"));

        SOURCE_LIST.add(SourceConstants.TAYNA_OKEANA, new Source("Тайна океана", "https://radiomayak.ru/podcasts/rss/podcast/1661/brand/59986/type/audio/"));
        SOURCE_LIST.add(SourceConstants.TOVARISH_TCIN, new Source("Товарищ Цинь", "https://radiomayak.ru/podcasts/rss/podcast/2261/brand/58219/type/audio/"));
        SOURCE_LIST.add(SourceConstants.TOLKOVIY_SLOVAR, new Source("Толковый словарь", "https://radiomayak.ru/podcasts/rss/podcast/2241/type/audio/"));

        SOURCE_LIST.add(SourceConstants.UTRO_KARL, new Source("Утро, Карл!", "https://radiomayak.ru/podcasts/rss/podcast/1901/brand/59605/type/audio/"));

        SOURCE_LIST.add(SourceConstants.PHILOSOPHY, new Source("Философия", "https://radiomayak.ru/podcasts/rss/podcast/1321/brand/59038/type/audio/"));

        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT, new Source("Хочу всё знать", "https://radiomayak.ru/podcasts/rss/podcast/1761/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_ASTRONOMIYA, new Source("Хочу всё знать. Астрономия", "https://radiomayak.ru/podcasts/rss/podcast/2422/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_BIOLOGIYA, new Source("Хочу всё знать. Биология", "https://radiomayak.ru/podcasts/rss/podcast/2421/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_GEOGRAFIYA, new Source("Хочу всё знать. География", "https://radiomayak.ru/podcasts/rss/podcast/2401/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_ISKUSSTVO, new Source("Хочу всё знать. Искусство", "https://radiomayak.ru/podcasts/rss/podcast/2426/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_ISTORIYA, new Source("Хочу всё знать. История", "https://radiomayak.ru/podcasts/rss/podcast/2363/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_KULTUROLOGIYA, new Source("Хочу всё знать. Культурология", "https://radiomayak.ru/podcasts/rss/podcast/2425/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_LITERATUROVEDENIE, new Source("Хочу всё знать. Литературоведение", "https://radiomayak.ru/podcasts/rss/podcast/2362/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_MATEMATIKA, new Source("Хочу всё знать. Математика", "https://radiomayak.ru/podcasts/rss/podcast/2361/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_OBZH, new Source("Хочу всё знать. ОБЖ", "https://radiomayak.ru/podcasts/rss/podcast/2404/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_OKRUZHAUSHIY_MIR, new Source("Хочу всё знать. Окружающий мир", "https://radiomayak.ru/podcasts/rss/podcast/2423/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_RAZVITIE_MISHLENIYA, new Source("Хочу всё знать. Развитие мышления", "https://radiomayak.ru/podcasts/rss/podcast/2162/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_RITORIKA, new Source("Хочу всё знать. Риторика", "https://radiomayak.ru/podcasts/rss/podcast/2365/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_RUSSKIY_YAZIK, new Source("Хочу всё знать. Русский язык", "https://radiomayak.ru/podcasts/rss/podcast/2381/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_FIZIKA, new Source("Хочу всё знать. Физика", "https://radiomayak.ru/podcasts/rss/podcast/2364/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_HIMIYA, new Source("Хочу всё знать. Химия", "https://radiomayak.ru/podcasts/rss/podcast/2402/brand/61222/type/audio/"));
        SOURCE_LIST.add(SourceConstants.HOCHU_VSE_ZNAT_SHKOLA_ROKA, new Source("Хочу всё знать. Школа рока", "https://radiomayak.ru/podcasts/rss/podcast/2641/brand/61222/type/audio/"));

        SOURCE_LIST.add(SourceConstants.CHTENIE, new Source("Чтение", "https://radiomayak.ru/podcasts/rss/podcast/703/type/audio/"));
        SOURCE_LIST.add(SourceConstants.CHTENIE_DLYA_DETEY, new Source("Чтение для детей", "https://radiomayak.ru/podcasts/rss/podcast/1741/type/audio/"));

        SOURCE_LIST.add(SourceConstants._23_FEVRALYA, new Source("23 февраля", "https://radiomayak.ru/podcasts/rss/podcast/261/type/audio/"));
        SOURCE_LIST.add(SourceConstants._30_SUZHETOV_CIVILIZACII, new Source("30 сюжетов цивилизации", "https://radiomayak.ru/podcasts/rss/podcast/2122/brand/60198/type/audio/"));
        SOURCE_LIST.add(SourceConstants._4_SEZONA, new Source("4 сезона", "https://radiomayak.ru/podcasts/rss/podcast/2301/brand/59986/type/audio/"));


        /*
            архивные записи
         */
        SOURCE_LIST.add(SourceConstants.AKUSTICHESKAYA_SREDA, new Source("Акустическая среда", "https://radiomayak.ru/podcasts/rss/podcast/105/brand/58262/type/audio/"));


    }

}








































