package er.mayak.mutabor.mayak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.List;

import er.mayak.mutabor.mayak.listener.RecyclerItemClickListener;
import er.mayak.mutabor.mayak.model.Source;
import er.mayak.mutabor.mayak.util.SourceConstants;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "TAG_" + getClass().getSimpleName();

    private RecyclerView mRecyclerView;
    private List<Source> data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = SourceConstants.SOURCE_LIST;
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        SourceAdapter adapter = new SourceAdapter(data);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,onItemClickListener));
    }


        private RecyclerItemClickListener.OnItemClickListener onItemClickListener = new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
                switch (position){
                    case SourceConstants.THE_CHILDREN:

                    case SourceConstants.ADMIRALTEYSTVO:
                    case SourceConstants.ASSAMBLEYA_AVTOMOBILISTOV:

                    case SourceConstants.BELAYA_STUDIA:
                    case SourceConstants.BOLSHOY_TEST_DRIVE:
                    case SourceConstants.BRENDYATINA:

                    case SourceConstants.VELIKIY_XIX:
                    case SourceConstants.VIA:
                    case SourceConstants.VNEKLASSNOE_4TENIE:

                    case SourceConstants.GOVORIM_PRAVILNO:

                    case SourceConstants.DK_URAL:

                    case SourceConstants.ZH_Z_I:

                    case SourceConstants.I_PUST_VES_HAIP_PODOZHDET:
                    case SourceConstants.IMENEM_REVOLUCII:
                    case SourceConstants.INOVESHANIE:

                    case SourceConstants.KAK_ZARABOTAT_MILLION:
                    case SourceConstants.KLINIKA_FADEEVA:
                    case SourceConstants.KLUB_RADIOPUTESHESTVENNIKOV:
                    case SourceConstants.KNIZHNAYA_POLKA:
                    case SourceConstants.KRASNAYA_MASHINA:
                    case SourceConstants.KRILYA_SOVETOV:
                    case SourceConstants.KURS_POVISHENIYA_ZNANIY:

                    case SourceConstants.LEKTORIUM:
                    case SourceConstants.LIKVIDACIYA_BEZGRAMOTNOSTI:
                    case SourceConstants.LITVIN:
                    case SourceConstants.LITERATURNIY_NOBEL:

                    case SourceConstants.MALENKIE_ISTORII_BOLSHOY_STRANI:
                    case SourceConstants.MASTERA_SPORTA_FUTBOL:
                    case SourceConstants.MAYAKOVSHINA_VNE_EFIRA:
                    case SourceConstants.MODA_STIL:
                    case SourceConstants.MODNAYA_SREDA:
                    case SourceConstants.MOZG:
                    case SourceConstants.MOSKVA_SLEZAM_POVERIT:
                    case SourceConstants.MOSKVOVEDENIE:
                    case SourceConstants.MUZH4INA_RUKOVODSTVO_PO_EKSPLUATACII:
                    case SourceConstants.MUZIKANTI:

                    case SourceConstants.NEIZVESTNOE_OB_IZVESTNOM:

                    case SourceConstants.O_VKUSNOY_I_ZDOROVOY_PISHE:
                    case SourceConstants.OB_ETOM_NE_PISALI_V_GAZETAH:

                    case SourceConstants.POL_KINO:
                    case SourceConstants.POPULARNAYA_EKONOMIKA:
                    case SourceConstants.PORA_DOMOY:
                    case SourceConstants.REBYATAM_O_ZVERYATAH:
                    case SourceConstants.RODITELSKIY_CHAS:
                    case SourceConstants.ROSA_VETROV:

                    case SourceConstants.SERGEY_STILLAVIN_I_EGO_DRUZYA:
                    case SourceConstants.SLADKAYA_ZHIZN:
                    case SourceConstants.SOBRANIE_SLOV:
                    case SourceConstants.SPUTNIK_KINOZRITELYA_S_ANTONOM_DOLINIM:
                    case SourceConstants.STAHOVSKIY_LIVE:
                    case SourceConstants.STRANA_TRANSISTORIYA:

                    case SourceConstants.TAYNA_OKEANA:
                    case SourceConstants.TOVARISH_TCIN:
                    case SourceConstants.TOLKOVIY_SLOVAR:

                    case SourceConstants.UTRO_KARL:

                    case SourceConstants.PHILOSOPHY:

                    case SourceConstants.HOCHU_VSE_ZNAT:
                    case SourceConstants.HOCHU_VSE_ZNAT_ASTRONOMIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_BIOLOGIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_GEOGRAFIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_ISKUSSTVO:
                    case SourceConstants.HOCHU_VSE_ZNAT_ISTORIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_KULTUROLOGIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_LITERATUROVEDENIE:
                    case SourceConstants.HOCHU_VSE_ZNAT_MATEMATIKA:
                    case SourceConstants.HOCHU_VSE_ZNAT_OBZH:
                    case SourceConstants.HOCHU_VSE_ZNAT_OKRUZHAUSHIY_MIR:
                    case SourceConstants.HOCHU_VSE_ZNAT_RAZVITIE_MISHLENIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_RITORIKA:
                    case SourceConstants.HOCHU_VSE_ZNAT_RUSSKIY_YAZIK:
                    case SourceConstants.HOCHU_VSE_ZNAT_FIZIKA:
                    case SourceConstants.HOCHU_VSE_ZNAT_HIMIYA:
                    case SourceConstants.HOCHU_VSE_ZNAT_SHKOLA_ROKA:

                    case SourceConstants.CHTENIE:
                    case SourceConstants.CHTENIE_DLYA_DETEY:

                    case SourceConstants._23_FEVRALYA:
                    case SourceConstants._30_SUZHETOV_CIVILIZACII:
                    case SourceConstants._4_SEZONA:


                        /*
                        далее идеут архивные записи
                         */

                    case SourceConstants.AKUSTICHESKAYA_SREDA:

                    intent.putExtra("url", data.get(position).getUrl());

                    break;
                }
                startActivity(intent);
            }
    };

}
