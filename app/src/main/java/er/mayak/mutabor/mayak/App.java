package er.mayak.mutabor.mayak;

import android.app.Application;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;


/**
 * Так сделано потому, что при открытии нового ItemActivity не закрывался плеер. Теперь он создается глобально.
 */
public class App extends Application {
    private SimpleExoPlayer mediaPlayer;

    public SimpleExoPlayer getGlobalMediaPlayer() {
        releasePlayer();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        mediaPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);
        return mediaPlayer;
    }

    public void releasePlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.setPlayWhenReady(false);
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

}
