package er.mayak.mutabor.mayak;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import er.mayak.mutabor.mayak.listener.RecyclerItemClickListener;
import er.mayak.mutabor.mayak.model.Item;

public class ItemActivity extends AppCompatActivity {
    private final String TAG = "TAG_" + getClass().getSimpleName();

    private RecyclerView mRecyclerView;
    private ItemAdapter adapter;
    private List<Item> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            data = new RetrieveFeedTask().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(data.size());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        adapter = new ItemAdapter(data);
        mRecyclerView.setAdapter(adapter);


        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,onItemClickListener));

    }



    class RetrieveFeedTask extends AsyncTask<String, Void,  List<Item>> {

        private String src;

       protected List<Item> doInBackground(String... urls) {
            List<Item> data = new ArrayList<>();
            Intent i = getIntent();
            src = i.getStringExtra("url");
            if(src==null){
                data.add(new Item("Ничего не найдено","","","","" ));
                return data;
            }
            URLConnection conn = null;
            try {
                URL url = new URL(src);
                conn = url.openConnection();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(conn.getInputStream());

                doc.getDocumentElement().normalize();

                NodeList nodeItemList = doc.getElementsByTagName("item");

            for (int temp = 0; temp < nodeItemList.getLength(); temp++) {

                Node nNode = nodeItemList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String title = eElement.getElementsByTagName("title").item(0).getTextContent();

                    /*
                    может не быть описания
                     */
                    NodeList nlDescription = eElement.getElementsByTagName("itunes:summary");
                    String description = nlDescription.getLength()>0 ? nlDescription.item(0).getTextContent() : "";

                    String mp3Url = eElement.getElementsByTagName("guid").item(0).getTextContent();
                    String pubDate = eElement.getElementsByTagName("pubDate").item(0).getTextContent();
                    String duration = eElement.getElementsByTagName("itunes:duration").item(0).getTextContent();

                    data.add(new Item(title, description, mp3Url, pubDate, duration));
                }
            }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  data;
       }

        protected void onPostExecute(Map<String, Map<String, String>> feed) {}

    }

    RecyclerItemClickListener.OnItemClickListener onItemClickListener = new RecyclerItemClickListener.OnItemClickListener() {
        @Override public void onItemClick(View view, int position) {
            Item item = data.get(position);
            Intent playerIntent = new Intent(getApplicationContext(), PlayerActivity.class);
            playerIntent.putExtra("title", item.getTitle());
            playerIntent.putExtra("url", item.getMp3Url());
            playerIntent.putExtra("duration", item.getDuration());
            startActivity(playerIntent);
        }
    };

}
