package er.mayak.mutabor.mayak;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import er.mayak.mutabor.mayak.model.Source;


public class SourceAdapter extends RecyclerView.Adapter<SourceAdapter.ItemHolder> {

    private final String TAG = "TAG_" + getClass().getSimpleName();

    List<Source> sourceList;

    SourceAdapter(List<Source> sourceList){
        this.sourceList = sourceList;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        CardView cv_source;
        TextView title;
        ItemHolder(View itemView) {
            super(itemView);
            cv_source = (CardView)itemView.findViewById(R.id.cv_source);
            title = (TextView)itemView.findViewById(R.id.source_title);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public SourceAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.source_list_view, parent, false);
        ItemHolder itemHolder = new ItemHolder(v);
        return itemHolder;
    }

    @Override
    public void onBindViewHolder(SourceAdapter.ItemHolder holder, int position) {
        holder.title.setText(sourceList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return sourceList.size();
    }
}

